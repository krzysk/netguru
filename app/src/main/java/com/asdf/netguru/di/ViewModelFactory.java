package com.asdf.netguru.di;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import com.asdf.netguru.persistence.CityDatabase;
import com.asdf.netguru.persistence.CityRepository;
import com.asdf.netguru.ui.SharedViewModel;

public class ViewModelFactory implements ViewModelProvider.Factory {
    private Application application;

    public ViewModelFactory(Application application) {
        this.application = application;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        CityDatabase db = Room.databaseBuilder(application.getApplicationContext(), CityDatabase.class, "db").build();
        return (T) new SharedViewModel(new CityRepository(db.cityDao()));
    }
}