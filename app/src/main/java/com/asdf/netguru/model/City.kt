package com.asdf.netguru.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.*

@Entity
data class City(val name: String, val color: String, @PrimaryKey val creationTimestamp: Long) {

    fun getDateTime() : String {
        val sdf = SimpleDateFormat("dd.MM.yyyy HH:mm:ss ", Locale.getDefault())
        return sdf.format(Date(creationTimestamp))
    }

}