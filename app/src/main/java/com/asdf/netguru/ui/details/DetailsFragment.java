package com.asdf.netguru.ui.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.asdf.netguru.R;
import com.asdf.netguru.databinding.FragmentDetailsBinding;
import com.asdf.netguru.model.City;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.gson.Gson;


public class DetailsFragment extends Fragment implements OnMapReadyCallback {

    private FragmentDetailsBinding binding;
    private City city;

    public static DetailsFragment newInstance(City city) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("city", new Gson().toJson(city));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (city != null) {
            CityDecorator newCity = new CityDecorator(city);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newCity.getLatLng(), 13.0f));
            newCity.getLatLng();
            binding.mapView.onResume();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false);
        setUpMaps(savedInstanceState);
        extractCity();
        return binding.getRoot();
    }

    private void setUpMaps(Bundle savedInstanceState) {
        binding.mapView.onCreate(savedInstanceState);
        binding.mapView.getMapAsync(this);
    }

    private void extractCity() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            String serializedCity = arguments.getString("city");
            if (serializedCity != null) {
                city = new Gson().fromJson(serializedCity, City.class);
            }
        }
    }
}
