package com.asdf.netguru.ui.main

import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.asdf.netguru.NetguruApplication
import com.asdf.netguru.R
import com.asdf.netguru.di.ViewModelFactory
import com.asdf.netguru.global.getColorInt
import com.asdf.netguru.model.City
import com.asdf.netguru.ui.SharedViewModel
import com.asdf.netguru.ui.details.DetailsFragment
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val sharedViewModel: SharedViewModel by viewModels { ViewModelFactory(application) }
    private val disposable = CompositeDisposable()
    private val first = "first"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        intent.extras?.getString(first)?.let {
            sharedViewModel.addCity(Gson().fromJson(it, City::class.java))
            intent.removeExtra(first)
        }

        sharedViewModel.cityOpenRequestEvent.observe(this, Observer {
            sharedViewModel.current = it
            showToolbarWithCityColor(it)
            loadDetails(it)
        })

        disposable.add((application as NetguruApplication).cityProducer.subscribe {
            sharedViewModel.addCity(it)
        })


        sharedViewModel.current?.let {
            showToolbarWithCityColor(it)
        }

        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            supportFragmentManager.popBackStack()
            sharedViewModel.current?.let {
                supportFragmentManager.beginTransaction().replace(R.id.details_host_fragment, DetailsFragment.newInstance(sharedViewModel.current)).commit()
            }
        } else {
            sharedViewModel.current = null
            showDefaultToolbar()
        }
    }

    private fun showToolbarWithCityColor(it: City) {
        supportActionBar?.title = it.name
        toolbar.setBackgroundColor(it.getColorInt())
    }

    override fun onBackPressed() {
        super.onBackPressed()
        showDefaultToolbar()
    }

    private fun showDefaultToolbar() {
        toolbar.setBackgroundColor(getColor(R.color.White))
        supportActionBar?.title = getString(R.string.app_name)
    }

    private fun loadDetails(city: City) {
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.details_host_fragment, DetailsFragment.newInstance(city)).commit()
        } else {
            supportFragmentManager.beginTransaction()
                .replace(R.id.nav_host_fragment, DetailsFragment.newInstance(city))
                .addToBackStack(null)
                .commit()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
}
