package com.asdf.netguru.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.asdf.netguru.NetguruApplication
import com.asdf.netguru.R
import com.asdf.netguru.ui.main.MainActivity
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable

class SplashActivity : AppCompatActivity() {

    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_splash)
        super.onCreate(savedInstanceState)
        val netguruApplication = application as NetguruApplication

        disposable.add(netguruApplication.cityProducer.subscribe {
            disposable.clear()
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("first", Gson().toJson(it))
            startActivity(intent)
            finish()
        })
    }
}