package com.asdf.netguru.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.asdf.netguru.global.SingleLiveEvent
import com.asdf.netguru.model.City
import com.asdf.netguru.persistence.CityDao
import com.asdf.netguru.persistence.CityRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class SharedViewModel(val cityRepository : CityRepository) : ViewModel() {

    val cityOpenRequestEvent = SingleLiveEvent<City>()

    var current : City? = null

    private val cities: MutableLiveData<List<City>> by lazy {
        MutableLiveData<List<City>>()
    }

    fun getCities(): LiveData<List<City>> {
        return cities
    }

    fun getFirstCity(): City? {
        return cityRepository.getCities().first()
    }

    fun addCity(city: City) {
        cityRepository.addCity(city)
        viewModelScope.launch(Dispatchers.IO) {
            cityRepository.insertCity(city)
        }
        cities.postValue(cityRepository.getCities())
    }

    fun cityClicked(city: City) {
        cityOpenRequestEvent.postValue(city)
    }

}