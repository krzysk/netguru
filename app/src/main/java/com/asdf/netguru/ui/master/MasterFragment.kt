package com.asdf.netguru.ui.master

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.asdf.netguru.databinding.FragmentMasterBinding
import com.asdf.netguru.ui.CityAdapter
import com.asdf.netguru.ui.CityAdapterViewModel
import com.asdf.netguru.ui.SharedViewModel
import kotlinx.android.synthetic.main.fragment_master.*

class MasterFragment : Fragment() {

    private val sharedViewModel: SharedViewModel by activityViewModels()
    private val cityAdapterViewModel: CityAdapterViewModel by activityViewModels()

    private lateinit var adapter: CityAdapter
    private lateinit var binding: FragmentMasterBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMasterBinding.inflate(inflater, container, false)
        binding.viewModel = sharedViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = CityAdapter(listOf(), cityAdapterViewModel)
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(activity)

        cityAdapterViewModel.openCityRequestedEvent.observe(viewLifecycleOwner, Observer {
            sharedViewModel.cityClicked(it)
        })

        sharedViewModel.getCities().observe(viewLifecycleOwner, Observer {
            adapter.updateCities(it)
        })
    }
}