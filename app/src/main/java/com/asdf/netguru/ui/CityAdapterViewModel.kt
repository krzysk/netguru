package com.asdf.netguru.ui

import androidx.lifecycle.ViewModel
import com.asdf.netguru.global.SingleLiveEvent
import com.asdf.netguru.model.City

class CityAdapterViewModel : ViewModel() {

    val openCityRequestedEvent = SingleLiveEvent<City>()

    fun cityClicked(city: City) {
        openCityRequestedEvent.postValue(city)
    }

}