package com.asdf.netguru.ui.details;

import com.asdf.netguru.model.City;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;

public class CityDecorator {

    Map<String, LatLng> map = new HashMap<>();

    private City city;

    public CityDecorator(City city) {
        this.city = city;
        map.put("Gdańsk", new LatLng(54.372158, 18.638306));
        map.put("Warszawa", new LatLng(52.237049, 21.017532));
        map.put("Poznań", new LatLng(52.409538, 16.931992));
        map.put("Białystok", new LatLng(53.13333, 23.16433));
        map.put("Wrocław", new LatLng(51.107883, 17.038538));
        map.put("Katowice", new LatLng(50.270908, 19.039993));
        map.put("Kraków", new LatLng(50.049683, 19.944544));
    }

    public LatLng getLatLng() {
        return map.get(this.city.getName());
    }

}
