package com.asdf.netguru.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.asdf.netguru.databinding.ItemCityBinding
import com.asdf.netguru.global.getColorInt
import com.asdf.netguru.model.City

class CityAdapter(private var cities: List<City>, private val listener: CityAdapterViewModel) :
    RecyclerView.Adapter<CityAdapter.CityViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCityBinding.inflate(inflater, parent, false)
        return CityViewHolder(binding, listener)
    }

    fun updateCities(cities: List<City>) {
        this.cities = cities
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return cities.size
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.bind(cities[position])
    }

    class CityViewHolder(private val binding: ItemCityBinding, private val listener: CityAdapterViewModel) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(city: City) {
            binding.model = city
            binding.listener = listener
            binding.city.setTextColor(city.getColorInt())
        }
    }


}