package com.asdf.netguru.persistence

import com.asdf.netguru.model.City

class CityRepository(val cityDao: CityDao) {

    private val cities = mutableListOf<City>()

    fun getCities(): List<City> {
        return cities.sortedWith(compareBy({ it.name }, { it.creationTimestamp }))
    }

    fun addCity(city: City) {
        cities.add(city)
    }

    suspend fun insertCity(city: City) {
        cityDao.insertCity(city)
    }
}