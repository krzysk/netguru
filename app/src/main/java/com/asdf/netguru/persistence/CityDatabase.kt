package com.asdf.netguru.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.asdf.netguru.model.City

@Database(entities = [City::class], version = 1)
abstract class CityDatabase : RoomDatabase() {

    abstract fun cityDao(): CityDao

}