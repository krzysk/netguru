package com.asdf.netguru.global

import android.graphics.Color
import com.asdf.netguru.model.City
import com.google.android.gms.maps.model.LatLng

val colorMap = mapOf<String, Int>(
    "Yellow" to Color.parseColor("#FFFF00"),
    "Green" to Color.parseColor("#00FF00"),
    "Blue" to Color.parseColor("#00FF00"),
    "Red" to Color.parseColor("#FF0000"),
    "Black" to Color.parseColor("#000000"),
    "White" to Color.parseColor("#FFFFFF")
)


fun City.getColorInt(): Int {
    return colorMap.getValue(color)
}
