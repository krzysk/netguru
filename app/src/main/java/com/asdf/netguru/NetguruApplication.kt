package com.asdf.netguru

import android.app.Application
import com.asdf.netguru.model.City
import io.reactivex.Observable
import io.reactivex.observables.ConnectableObservable
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class NetguruApplication : Application() {

    private val cities =
        listOf("Gdańsk", "Warszawa", "Poznań", "Białystok", "Wrocław", "Katowice", "Kraków")
    private val colors = listOf("Yellow", "Green", "Blue", "Red", "Black", "White")

    private val citySource = Observable.interval(5, TimeUnit.SECONDS).flatMap {
        Observable.just(
            City(
                cities[Random.nextInt(cities.size)],
                colors[Random.nextInt(colors.size)],
                System.currentTimeMillis()
            )
        )
    }
    val cityProducer: ConnectableObservable<City> = citySource.publish()

    override fun onCreate() {
        super.onCreate()
        cityProducer.connect()
    }
}